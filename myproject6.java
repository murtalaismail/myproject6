/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//Program to generate random numbers and use return wheather even or odd
package randnumber;

/**
 *
 * @author user
 */
public class RandNumber {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // This program generates random numbers and checks whether number generated is even or odd.
        int number = 0;
        number = 1+(int)(10*Math.random());
        if (number%2 == 0){
            System.out.println("The number "+number+" you got is even.");
        }
        else{
            System.out.println("The number "+number+" you got is odd.");
        }
    }   
}